<?php

namespace Athlon;

class Card
{
    public $suit;
    public $rank;

    public function __construct(string $suit, int $rank)
    {
        if (!$rank || $rank < 1 || $rank > 13) {
            return false;
        }
        if (!$suit || !in_array(strtolower($suit), ['hearts', 'spades', 'clubs', 'diamonds'])) {
            return false;
        }
        $this->suit = ucfirst(strtolower($suit));
        $this->rank = $rank;
    }

    public function faceCard()
    {
        return $this->rank > 10;
    }

    public function __toString()
    {
        switch ($this->rank) {
            case 1:
                return 'Ace of '.$this->suit;
            case 11:
                return 'Jack of '.$this->suit;
            break;
            case 12:
                return 'Queen of '.$this->suit;
            break;
            case 13:
                return 'King of '.$this->suit;
            break;
        }
        // 2-10
        return $this->rank.' of '.$this->suit;
    }
}