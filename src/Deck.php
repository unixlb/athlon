<?php

namespace Athlon;

//require "../vendor/autoload.php";

use Athlon\Card;

class Deck 
{
    public $cards = [];

    public function __construct()
    {
        foreach (['hearts', 'diamonds', 'spades', 'clubs'] as $suit) {
            for ($rank = 1; $rank <= 13; $rank++) {
                $this->cards[] = new Card($suit, $rank);
            }
        }
    }

    public function count()
    {
        return count($this->cards);
    }

    public function shuffle()
    {
        shuffle($this->cards);
    }

    public function draw(int $count)
    {
        return array_slice($this->cards, -$count);
    }
}