<?php

use \PHPUnit\Framework\TestCase;
use Athlon\Deck;

class DeckTest extends TestCase
{

    public function testCountOfTheDeck()
    {
        $deck = new Deck();
        $this->assertEquals(52, $deck->count());
    }
    
    public function testDraw()
    {
        $deck = new Deck();
        $this->assertIsArray($deck->draw(1));
    }

    public function testShuffllingTheDeck()
    {
        $deck = new Deck();
        $deck->shuffle();
        $this->assertNotEquals(['Hearts', 1], [$deck->cards[0]->suit, $deck->cards[0]->rank]);
    }
}