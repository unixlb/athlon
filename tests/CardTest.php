<?php

use \PHPUnit\Framework\TestCase;

class CardTest extends TestCase
{
    public function testCreateACard()
    {
        $card = new \Athlon\Card('hearts', 5);

        $this->assertEquals('Hearts', $card->suit);
        $this->assertEquals(5, $card->rank);
    }

    public function testCompareTwoCards()
    {
        $card1 = new \Athlon\Card('hearts', 5);
        $card2 = new \Athlon\Card('spades', 7);
        $this->assertGreaterThan($card1->rank, $card2->rank);
    }
}